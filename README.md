# palworld-custom-calc

### Install
```
python -m venv .
source bin/activate
pip install poetry
poetry install
```

### Running
```
poetry run python calc.py # This will drop you into an input for what child you want to search parents for
poetry run python calc.py --mode reverse --child cryolinx
poetry run python calc.py --mode parents --parents helzephyr,cryolinx
```

### What is `all_combos.csv`
A direct download from the Google Doc containing All Combos of all breeding
with only a slight data-cleaning tweak.
