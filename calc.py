import argparse
from itertools import product

import pandas as pd


def get_first(iterable):
    return iterable[0]


def prune_permutations(matches: list) -> list:
    pruned_matches = list()
    for match in matches:
        if sorted(match) in pruned_matches:
            continue
        else:
            pruned_matches.append(sorted(match))

    return pruned_matches


def reverse_child_search(df: pd.DataFrame, child: str = ""):
    if not child:
        desired_child = input("Search desired child\n> ").lower().title()
    else:
        desired_child = child.lower().title()
    match_array = df[df.isin([desired_child]).any(axis=1)].index.values
    confirmed_matches = list()
    for pairing in list(product(match_array, match_array)):
        parent1, parent2 = pairing
        # We have to offset by 1 because we dropped a whole column
        if get_first(df.take([parent1]).iloc[:, parent2 + 1].values) == desired_child:
            confirmed_matches.append([parent1, parent2])

    for match in prune_permutations(confirmed_matches):
        parent1, parent2 = match
        print(
            f"{df.iloc[:, parent1+1].name} + {df.iloc[:, parent2+1].name} = {desired_child}"
        )


def parents_search(df: pd.DataFrame, parents: str):
    df = pd.read_csv("all_combos.csv")
    parent1, parent2 = [parent.lower().title() for parent in parents[0].split(",")]
    child = get_first(df[(df["pName"].isin([parent1]))][parent2].values)
    print(f"{parent1} + {parent2} = {child}")


def load_local_csv():
    df = pd.read_csv("all_combos.csv")
    df = df.drop(labels="pName", axis=1)
    return df


def load_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--mode",
        choices=["reverse", "parents"],
        help="Switches the mode of the program",
        default="reverse",
        type=str,
    )
    parser.add_argument(
        "--child",
        required=False,
        help="Immediately set the desired child rather than going through input",
        type=str,
    )
    parser.add_argument(
        "--parents",
        required=False,
        help="Immediately set the two parents to calculate breeding for",
        nargs="*",
    )
    return parser.parse_args()


def main():
    df = load_local_csv()
    args = load_args()
    if args.mode == "reverse":
        reverse_child_search(df, args.child)
    else:
        parents_search(df, args.parents)


if __name__ == "__main__":
    main()

### BELOW CONTAINS A SHIT TON OF INFORMATION EXPLAINING THIS INSANITY
"""
df[df.isin(["Orserk"])].stack()
ipdb> import sqlite3
ipdb> cnx = sqlite3.connect(':memory:')
ipdb> cur = cnx.cursor()
ipdb> cur.fetchall()
# find pal given both parents
df[(df["pName"].isin(["Cattiva"]))]["Jetragon"]

# find parents given child pal name
ipdb> df[df.isin(["Orserk"]).any(axis=1)]

# get nth row
df.take([n])

# get nth columns
df.iloc[:, n]

suppose we have our typical matrix of:
    [[coulmnpal1, columnpal2, columnpal3],
     [rowpal1, pal1, pal2, pal3],
     [rowpal2, pal4, pal5, pal6],
     [rowpal3, pal7, pal8, pal9]]

    In the context of breeding this would mean that columnpal2 bred with rowpal2 would equal pal5

    Now that we have the basic structure out of the way it is a true statement that the series of
    columnpalN is the series of rowpalN, therefore we can drop at least one. I choose rowpal.

    Now we have
    [[coulmnpal1, columnpal2, columnpal3],
     [pal1, pal2, pal3],
     [pal4, pal5, pal6],
     [pal7, pal8, pal9]]

    This now means that a pal bred of columnpal2 and row N=3 is actuall columnpal2, columnpal3
    which would equal pal8

    This is a really complicated way of saying that if you are doing a feasibility breadth-first search
    or "What parents make Orserk?"

    You would do
        df[df.isin(["Orserk"]).any(axis=1)]

    This will return indices 101,127, and 131

    What does that mean? It means you grab the 101st column and the first name is the name of that pal IE
        Relaxaurus

    Now given all indices we have to go through them all to find parents.
    This is because we now that 101,127,131 somehow make Orserk but how? 101 and 101? 131 and 127?

    The simplest answer here is to build a certesian product of the options and brute force the result
        a = b = [101, 127, 131]
        pairings = list(product(a, b))
    >>> for pairings in list(product(a,b)):
    ...     parent1, parent2 = pairings
    ...     print(df.take([parent1]).iloc[:, parent2].name)

    And now if the pairing equals our searched-for child - we have a hit!

"""
